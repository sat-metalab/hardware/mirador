MIRADOR
-------

Module d'Immersion en Réalité Augmentée Déployable et Opérant en Réseau.


What it is
==========

Mirador is a device facilitating the prototyping and deployment of immersive, interactive experiences. We envision to have multiple flavours of Mirador depending on the context, with the common goal of making it easier to experiment with collective immersion anywhere, particularly in places not equipped with permanent setups.

[![click](doc/images/mirador_vimeo.jpg)](https://vimeo.com/549423960)

<p><a href="https://vimeo.com/549423960">Mirador</a> from <a href="https://vimeo.com/satmetalab">Metalab | SAT</a> on <a href="https://vimeo.com">Vimeo</a>.</p>


Why are we doing this?
======================

The goal of Mirador is to enable the creation of prototypes for social and/or collective immersion.

Social and collective immersion are setups where immersive media are overlayed onto the physical space. This includes installations like fulldomes, but also immersive exhibitions of famous painters, where their scanned masterpieces are projected onto the walls of a room.

Social and collective immersion usually requires non-trivial amounts of hardware and software, which often needs to be installed in a dedicated setting. The Mirador project aims to make these kinds of immersion more flexible and adaptable, enabling fast prototyping of novel immersive experiences in new settings.


What is inside
==============

Currently Mirador is meant to run [Splash](https://sat-metalab.gitlab.io/splash) and [LivePose](https://sat-metalab.gitlab.io/livepose) to project video immersion and detect participants poses and actions.

On the hardware side it currently involves a [NVIDIA Jetson Xavier NX](https://www.nvidia.com/en-us/autonomous-machines/embedded-systems/jetson-xavier-nx/) connected to a LED portable projector and an [Intel Realsense camera](https://www.intelrealsense.com/). With a projector and a camera, this system can be calibrated as a stereoscopic pair (commonly called `procam`).

![First prototype](doc/images/mirador_first_prototype.jpg)


The shell
=========

The structure of Mirador is made so that it becomes difficult to notice in the context of an installation. More information about the outer shell and the inner structure can be found in the [maker notebook](doc/mirador-maker-notebook.pdf).


Building
========

The two first prototypes have been built using rapid prototyping methods. Overall a secondary goal is to make it possible for anyone to reproduce the device, be it the hardware or the software. Here are a few photographies of the construction process.

![Wood parts](doc/images/wood_parts_cut.jpg)

![Inner assembly](doc/images/inner_assembly.jpg)

![Inner assembly - side](doc/images/inner_assembly_side.jpg)

![Painted - unmounted](doc/images/painted_unmounted.jpg)

Resources
=========

You may want to know more about the processes and materials involved in the making of that project. You can refer to [tools & credits](doc/tools+credits.md) for the software and visual assets used.

Software
========

The software is currently a work in progress, as there is no ready to use distribution which holds all the tools and gives an easy to use user interface. This is a work in progress though as seen by the [packaging work being done at the Metalab](
https://gitlab.com/sat-metalab/distributions).

License
=======

This project is released under [CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/).

This project is made possible thanks to the Society for Arts and Technologies (also known as SAT).
